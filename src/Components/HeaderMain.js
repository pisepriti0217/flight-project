import React from "react";
import headerMain from '../CSS Files/headerMain.css';

function HeaderMain() {
    return (
            <div className="main">
            <div className="header">
                <a href="#default" className="logo">Flight-Portal</a>
                <div className="header-right">
                <ul className="nav navbar-nav navbar-right">
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#home">HOME<span span className="caret"></span></a>
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#pages">PAGES</a>
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#tours">TOURS</a>
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#blog">BLOG</a>
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#shop">SHOP</a>
                    <a className="dropdown-toggle" data-toggle="dropdown" href="#elements">ELEMENTS</a>    
                </ul>                
                </div>
            </div>
        </div>  
    )
}

export default HeaderMain