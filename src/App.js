import React from 'react';
import MidComponent from './Components/MidComponent';
import HeaderMain from './Components/HeaderMain.js';

function App() {
  return (
    <div>
      <HeaderMain/>
      <MidComponent/>
    </div>
    );
}

export default App;
